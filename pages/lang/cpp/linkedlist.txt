====== LinkedList (C++) ======

<file c++ linkedlist.cc>
/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cstdio>
#include <vector>

class Record {
  public:
    int value;
    Record *prev;
    Record *next;

    Record(int _value, Record *_prev, Record *_next)
      :value(_value), prev(_prev), next(_next) {
    }

    explicit Record(int _value) :value(_value), prev(NULL), next(NULL) {
    }
};

class LinkedList {
  public:
    Record *first;  // first record in LL
    Record *last;   // last record in LL
    Record *iter;   // actual position in LL

    LinkedList() {
      this->first = NULL;
      this->last  = NULL;
      this->iter  = NULL;
    }

    explicit LinkedList(std::vector<int> v) {
      this->first = NULL;
      this->last = NULL;
      this->iter = NULL;
      for (unsigned int i = 0; i < v.size(); i ++)
        this->add(v[i]);
    }

    explicit LinkedList(LinkedList *ll) {
      this->first = NULL;
      this->last  = NULL;
      this->iter  = NULL;

      for (Record *r = ll->first; r != NULL; r = r->next)
        this->add(r->value);
    }

    ~LinkedList() {
      Record *r = this->first;
      Record *tmp;
      while (r != NULL) {
        tmp = r->next;
        delete r;
        r = tmp;
      }
    }

    void add(int value) {
      Record *r = new Record(value);

      if (this->first == NULL) {
        this->first = r;
        this->last  = r;
        this->iter  = r;
        r->next = NULL;
        r->prev = NULL;
      } else {
        this->last->next = r;
        r->prev = this->last;
        r->next = NULL;
        this->last = r;
      }
    }

    void append(std::vector<int> v) {
      for (unsigned int i = 0; i < v.size(); i++)
        this->add(v[i]);
    }

    int size() {
      int size = 0;
      Record *r = this->first;
      while (r != NULL) {
        r = r->next;
        size++;
      }
      return(size);
    }

    void clear() {
      Record *r = this->first;
      Record *tmp;
      while (r != NULL) {
        tmp = r->next;
        delete r;
        r = tmp;
      }
      this->first = NULL;
      this->last = NULL;
      this->iter = NULL;
    }

    Record* begin() {
      return(this->first);
    }

    Record* end() {
      return(this->last);
    }
};


void revert(LinkedList *ll) {
  LinkedList *newll = new LinkedList();
  for (Record *r = ll->last; r != NULL; r = r->prev)
    newll->add(r->value);
  ll->clear();
  for (Record *r = newll->first; r != NULL; r = r->next)
    ll->add(r->value);
  delete(newll);
}

void bubblesort(LinkedList *ll) {
  LinkedList *sorted = new LinkedList(ll);
  bool swaped;
  do {
    swaped = false;
    Record *r = sorted->first;
    while (1) {
      if (r->next == NULL)
        break;

      if (r->value > r->next->value) {
        printf("swap %i & %i\n", r->value, r->next->value);
        // R0 - R1 - R2 - R3
        Record *R0 = r->prev;
        Record *R1 = r;
        Record *R2 = r->next;
        Record *R3 = R2->next;
        if (R0 != NULL) R0->next = R2;
        R2->next = R1;
        R1->next = R3;
        if (R3 != NULL) R3->prev = R1;
        R1->prev = R2;
        R2->prev = R0;
        swaped = true;
        if (R1 == sorted->first) {
          sorted->first = R2;
        }
      } else {
        r = r->next;
      }
    }
  } while (swaped);

  ll->clear();
  for (Record *r = sorted->first; r != NULL; r = r->next)
    ll->add(r->value);
  delete(sorted);
}

int main() {
  std::vector<int> v;
  v.push_back(0);
  v.push_back(1);
  LinkedList *ll = new LinkedList(v);
  ll->add(2);
  ll->add(3);
  ll->add(4);
  v.clear();
  v.push_back(5);
  v.push_back(6);
  v.push_back(7);
  ll->append(v);
  printf("Size %i\n", ll->size());
  Record *r;
  // dump forth
  for (r = ll->first; r != NULL; r = r->next)
    printf("%i ", r->value);
  printf("\n");
  // dump backward
  for (r = ll->last; r != NULL; r = r->prev)
    printf("%i ", r->value);
  printf("\n");
  // revert
  revert(ll);
  // dump
  for (r = ll->first; r != NULL; r = r->next)
    printf("%i ", r->value);
  printf("\n");
  // sort
  bubblesort(ll);
  // dump
  for (r = ll->first; r != NULL; r = r->next)
    printf("%i ", r->value);
  printf("\n");
  ll->clear();
  printf("Size %i\n", ll->size());
  delete ll;
  return(0);
}

/* EOF */
</file>
