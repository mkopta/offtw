====== Pipe skip ======
===== Problém =====
Chci přeskočit určitý počet snímků nekomprimovaného videa ve formátu yv12.
===== Script =====
skip-frames.pl
<file perl skip-frames.pl>
#!/usr/bin/perl
use strict;
use warnings;
my ($width,$height,$skip) = @ARGV;

my $frame_size = int ($width * $height * 6/4);

open (my $input, '<:raw', 'fif') or die 'Can not open fifo for reading';
open (my $output, '>:raw', 'fif2') or die 'Can not open fifo for writing';

my $num = 0;
$| = 1;

local $/ = \$frame_size;
while (defined (my $line = <$input>) && $skip--) {
        print "\rSkipping frame: " . ++$num . "\e[K";
}
print "\n$num reached \e[K\n";
while (<$input>) {
        print $output $_;
        print "\rSending frame " . ++$num . "\e[K";
}
print "\nDone\n";
$| = 0;

close $input;
close $output;
</file>

===== Použití =====
<code>
# vstupni pipe
$ mkfifo fif
# vystupni pipe
$ mkfifo fif2
$ perl skip-frames.pl 720 576 5000
</code>
Přeskočí prvních 5000 snímků, zbytek odešle do ''fif2''.

===== Výhody =====
  * Jednoduchost

===== Nevýhody =====
  * Je to hromada hnoje.
