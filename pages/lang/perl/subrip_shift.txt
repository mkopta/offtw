====== Subrip shift ======

===== Popis =====
Posune časové značky v subrip titulcích o stanovenou hodnotu.

===== Použití =====
Posune titulky o 24s ke konci souboru, převede z cp1250 do UTF-8.
<code>
perl subrip_shift.pl -i WINDOWS-1250 -s 24 file1.srt file2.srt
</code>
Další parametr je ''-o'' pro výstupní encodování. Defaultní encodování je UTF-8.
Parametr -s je povinný. Lze použít i v rouře.

===== Code =====
<file perl subrip_shift.pl>
#!/usr/bin/env perl
## Author: Robert Hülle
## License: WTFPL v2

use strict;
use warnings;
use 5.010;

use Encode;
use Getopt::Long;

my %option;
GetOptions(\%option,
  'input-encoding|i=s',
  'output-encoding|o=s',
  'shift=f',
) or die "Wrong usage\n";

die "Missing shift\n" unless exists $option{shift};

my $in_enc = find_encoding($option{'input-encoding'} // 'UTF-8');
my $out_enc = find_encoding($option{'output-encoding'} // 'UTF-8');

unless ((defined $in_enc) && (defined $out_enc)) {
  die "encoding not found\n";
}

$^I = '.bak';
while (<>) {
  chomp;
  my $line = $in_enc->decode($_);
  if ( my ($ah, $am, $as, $ass, $bh, $bm, $bs, $bss)
    = m/^(\d\d):(\d\d):(\d\d),(\d\d\d)\s-->\s(\d\d):(\d\d):(\d\d),(\d\d\d)/ ) {
    my $time = $ah*3600 + $am*60 + $as + $ass / 1000;
    $time += $option{'shift'};
    $ah = int($time / 3600);
    $am = int(($time % 3600) / 60);
    $as = int(($time % 3600) % 60);
    $ass = ($time*1000) % 1000;
    $time = $bh*3600 + $bm*60 + $bs + $bss / 1000;
    $time += $option{'shift'};
    $bh = int($time / 3600);
    $bm = int(($time % 3600) / 60);
    $bs = int(($time % 3600) % 60);
    $bss = ($time*1000) % 1000;
    $line = sprintf '%02d:%02d:%02d,%03d --> %02d:%02d:%02d,%03d',
      $ah, $am, $as, $ass,
      $bh, $bm, $bs, $bss,
  }
  say $out_enc->encode($line);
}

__END__
</file>