====== Tcl - větvení ======
[[lang:tcl:tcl|Tcl index]]
===== podmíněné vykonání příkazu =====
  * if
<code tcl>
if { $x > 42 } {
  set d "vetsi"
} elseif { $x < 42 } {
  set d "mensi"
} else {
  set d "rovno"
}
</code>
  * switch
<code tcl>
switch $str {
  "rano" { set d "snidane" }
  "poledne" { set d "obed" }
  "vecer" { set d "vecere" }
  default { set d "svacina" }
}
</code>