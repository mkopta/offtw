====== Slovíčka z učebního textu Leben mit Informatik ======

<code>
abstimmen=vyladit,naladit
sich abzeichnen=ukazovat se,projevovat se
allgegenwärtig=všudypřítomný
Der Alltagsgegenstand=předmět denní potřeby,všední věc
anschaulich=názorný,názorně
Die Anstrengung=úsilí,námaha
Die Anwendung=použití,uplatnění,upotřebení
Die Auswirkung=důsledek,následek,účinek
beeinflussen=ovlivnit,působit,zapůsobit
bereitstellen=chystat,připravit,dát k dispozici
Das Datenfunknetzwerk=bezdrátové spojení,bezdrátová síť,internet
deutlich=zřetelný,zřejmý
durchschnittlich=průměrný
empfangen=přijímat
Der Energiestoffwechsel=energetický metabolismus
Das Entertainment=zábava
Der Fortschritt=pokrok
gegenwärtig=přítomný,současný
Die Gestaltung=utváření,podoba
Die Gesellschaft=společnost
Die Grundlage=základ
Der Haushalt=domácnost
Die Herausforderung=výzva
Die Mediengattung=mediální druh,žánr
Die Neugier=zvědavost
nutzen=užít,využít
Die Öffentlichkeit=veřejnost
Das Rückgrat=páteř
Das Schlüsselwort=klíčové slovo,heslo,kód
sich verändern=změnit se
Die Sicherheit=bezpečí,bezpečnost,jistota
Der Strom=proud
sukzessiv,sukzessive=postupný,postupně
überwachen=hlídat,dohlížet,kontrolovat
unbemerkt=nepozorovaný,nepozorovaně
verbrauchen=spotřebovat
vermeiden=vyhnout se,zabránit
Die Verschmelzung=sloučení,splynutí
vorkommen=zdát se
Der Wandel=změna,proměna,obrat
Beachtung finden=dojít povšimnutí,dojít pozornosti
bei Bedarf=v případě potřeby
Den Blick auf etwas richten=pohlédnout na něco,upřít zrak na něco
Einzug halten=vstupovat,přijíždět kam
Geschäfte abwickeln=realizovat,uzavřít obchody,obchodovat
</code>