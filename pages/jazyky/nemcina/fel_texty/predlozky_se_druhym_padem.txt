====== Předložky se druhým pádem (Němčina) ======

<code>
angesichts=vzhledem
anlässlich=u příležitosti
anstatt,statt=namísto
aufgrund=na základě
ausserhalb=mimo
bezüglich=ohledně,co se týče,co se týká
binnen=během,do
einschliesslich=včetně
infolge=v důsledku
innerhalb=uvnitř,v
mittels=s pomocí,pomocí
seitens=ze strany
trotz=přes,navzdory
ungeachtet=nehledě
unweit=nedaleko
während=během
wegen=kvůli
zwecks=za účelem
</code>
