====== Francouzština pro samouky, poznámky - Lekce 3. ======
===== Gramatika =====
==== 1. Sloveso être - být ====

| **je suis** | já jsem | **nous sommes** | my jsme |
| **tu es** | ty jsi | **vous êtes** | vy jste |
| **il est** | on je | **ils sont** | oni jsou |
| **elle est** | ona je | **elles sont** | ony jsou |

==== 2. Otázka inverzí ====
  * podmět se přesune za sloveso 

| Qui **êtes-vous**? | Kdo jste? |
| Paul, où **es-tu**? | Kde jsi, Pavle? |
| Où **sont-ils**? | Kde jsou? |

==== 3. Člen ve jménném přísudku ====
  * podstatná jména po slovese être vyjadřující zaměstnání nemají člen (Vous êtes médicin?)
  * po výrazu c'est, ce sont má podstatné jméno člen neurčitý (C'est un étudiant?)

==== 4. Předložka u jmen zemí a měst ====
  * u jmen zemí ženského rodu se používá předložka **en** (en France)
  * u jmen měst se používá předložka **à** (à Paris)

==== 5. Nepravidelné sloveso faire ====

| **tu fais** | děláš |
| **vous faites** | děláte |

===== Slovíčka =====
Zdrojový soubor programu KWordQuiz: {{:jazyky:francouzstina:samouci:lekce_03.kvtml}}
<code>
trois	tři
contente	spokojená
maman	maminka
la chambre	pokoj
ta chambre	tvůj pokoj
le balcon	balkon
j'espère	doufám
sous	pod
le divan	pohovka
pourquoi	proč
qu'est-ce que	co
papa	tatínek
la salle à manger	jídelna
le placard	skříň
mon Dieu!	proboha!
nous jounons	hrajeme
ah bon!	ach tak
bien sûr	samozřejmě
toujours	vždy, pořád
des bêtises	hlouposti
pardon	promiň
de	z, ze
le médicin	lékař, lékařka
l'étudiant	student
le Français	Francouz
le Tchèque	Čech
l'ouvrier	dělník
maintenant	teď
en	v, ve
la France	Francie
n'est-ce pas?	že?
l'ingénieur	inženýr
chez	u
Je ne sais pas.	Nevím.
Où es-tu?	Kde jsi?
Qu'est-ce que tu fais?	Co děláš?
Qu'est-ce que vous faites?	Co děláte?
Vous êtes médicin?	Jste lékař?
Vous êtes tchèque?	Jste Čech?
Je vois.	Už vím.
Ça va?	Jak se máš?
Ça va bien.	Mám se dobře.
Pas mal.	Ujde to.
</code>