====== Parler - mluvit ======
Jedná se o sloveso první třídy - infinitiv je zakončen na -**er**.

|je parl**e** |nous parl**ons** |
|tu parl**es** |vous parl**ez** |
|il parl**e** |ils parl**ent** |
