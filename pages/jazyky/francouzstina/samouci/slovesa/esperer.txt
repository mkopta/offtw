====== Espérer - doufat ======
  * před infinitivní koncovkou -**er** mají zavřené **é**, před němou koncovkou (-**e**, -**es**, -**ent**) dostává do přízvučné slabiky a mění se v otevřené **è**

|j'esp**è**re |nous esp**é**rons |
|tu esp**è**res |vous esp**é**rez |
|il esp**è**re |ils esp**è**rent |