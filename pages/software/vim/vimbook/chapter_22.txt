====== Poznámky z Vimbooku - kapitola 22. ======

(VISUAL mód)

  * Lze používat ''d D y Y ..''
  * ''$'' skočí na konec řádku (permanentně)
  * ''gv'' označí oblast vybranou v minulém výběr (**!!!**)
  * ''aw'' označí další slovo, ale jde o jinou věc než ''w''. Funguje i na druhý směr a je inteligentnější než ''w'' pokud jde o VISUAL. Podobný příkaz je ''iw'' (inner word).
  * ''as'' vybere větu
  * ''gap'' vybere odstavec (''ip'')
  * Text do další závorky
    * ''a('' nebo ''i(''
      * ''vi('' vybere obsah závorky (a nezáleží na pozici kurzoru) (podobné jako ''ci('')
    * ''a<'' nebo ''i<''
    * ''a['' nebo ''i[''
    * ''a{'' nebo ''i{''
  * Písmeno ''o'' prohodí kurzor na druhý konec výběru
    * **UŽITEČNÉ**
    * ''O'' prohodí kurzor na druhou stranu (roh) výběru (hodí se u sloupcového výběru)
  * Velikost písmen
    * ''u'' změní všecha písmena ve výběru na malá
    * ''U'' změní všecha písmena ve výběru na velká
    * ''~'' změní všecha písmena ve výběru na opačnou velikost
  * Spojování řádek
    * ''J'' spojí vybrané řádky
    * ''gJ'' spojí vybrané řádky bez mezery
  * Formátování textu ''gq'' (celkem častá operace)
  * Rotace13 ''g?''
  * Vykonání příkazu '':''
  * Vykonání externího příkazu nad vybraným textem (filtr) ''!''. Např.: ''!sort''
  * Select mode
    * ''gh'' znakový
    * ''gH'' řádkový
    * ''g^H'' blokový
    * Select mode je jiný než VISUAL mód, neumožňuje příkazy, backspace maže, jakékoliv písmeno přepíše výběr, pro pohyb je potřeba používat šipky (podobné klasickému selectu z jiných aplikací)
    * Pro přepnutí VISUAL <-> SELECT ''^g''
