====== Poznámky z Vimbooku - kapitola 99. ======

Tyto poznámky jsou zcela chaotické, neuspořádané a jejich původ je neznámý (mám je na papírech, ale proč a kde a kdy se tam vzali nevím). Jejich souvislost s Vimbookem je neurčitá. Jejich užitečnost taktéž. Většina tipů pravděpodobně vykrádá cizí nápady (protože to nejsou moje nápady).

  * '':perldo s/something/other/'' substituce pomocí interpreteru perlu
  * '':help regexp''
  * ''"_d'' mazání do ''/dev/null'' registru
  * ''"*'' registr mělké schránky xserveru (middle click), ''"+'' registr hluboké schránky xserveru (ctrl-c), ''"='' expression registr (možnost výpočtů)
  * Výběr objektů nezávisle na pozici kurzoru ve VISUAL
    * ''aw'', ''iw'', ''w'', ''w<'', ''a<..>'', ''i<..>''
  * ''help iskeyword'', '':set iskeyword=?'', ''"=&iskeyword<enter>p''
  * ''help quote''
  * Firefox
    - ''about:config''
    - ''view_source.editor.external  true''
    - ''view_source.editor.path      /usr/bin/gvim''
  * ''ya('' zkopíruje (yank) obsah celé závorky ve které stojí (někde) kurzor
  * '':set list'' ('':help list'')
  * '':retab'' ('':help retab'')
  * Jsme v insert mode a pomocí ''^o<příkaz>'' máme možnost vykonat jeden příkaz typu normal mode (př.: ''^odd'')
  * '':set hls'' a '':set nohls''
  * Undo nemusí být jen krokové, ale i časové
    * '':earlier 10m''
    * '':help undo-tree''
  * ''ga'' ukáže ASCII hodnotu znaku pod kurzorem
  * '':set mouse=a'' umožní interakci s myší (různé další schopnosti)
  * Skoky po slovech - ''n b w N B W''
  * Editace sloupce -> ''^v'' -> ''I''
  * R, r - replace
  * '':set showcmd'' <- zobrazování příkazu při jeho psaní ve statusbar
  * ''gq}'' -> gq = formátuj, } = paragraf
  * ''^a'', ''^x'' inkrementace a dekrementace, desítková, osmičková, binární, hexadecimální, alfanumerická, .. '':help nrformats''
  * Substituce ve VISUAL ''^v'' -> ''%%:s///%%''
  * '':set paste'' mimo jiné vypne všechno odsazování
  * ''=='' v normal/visual reformátuje selekci (brutal ultra mega cool -> ''%='')
  * místo ''ESC'' jde ''^['' (a milión dalších způsobů)
  * ''^o'' (v normal) zpět jeden krok v historii skoků v editoru (i přes soubory)
  * '':registers'' vypíše seznam registrů a jejich obsahy
  * skoky po závorkách -> ''# % *'' a podobně
  * '']I'' vyhledává výskyty i ve všech includovaných (ale stejně je lepší ''^wi'')
  * Při kompilaci vimu dát ke configure skriptu parametr ''--enable-perlinterp'' pro podporu perlu ve vimu
  * Dva apostrofy jsou skok zpět (dvakrát jednoduchá uvozovka)
  * ''^i^o'' je také skok zpět, více v '':help jump-motions''
  * '':undol'' vypíše větve historie
    * ''g-'' a ''g+'' posunuje v čase
    * '':earlier 15m'' taky
  * '':help ex-cmd-index'' kompletní stručný přehled všech dvojtečkových příkazů
  * Vymazání nějakého kusu textu pomocí substituce
    - '':%s/\v(foo)(bar)/\1/''
    - ''%%:%s/\v(foo)@<=(bar)//%%'' (všimněte si, že to vypadá jako ukazatel do černé díry
    - ''%%:%s/\v(foo)\zs(bar)//%%'' tohle je taková zkratka předchozího
    * '':help /\@=<''
    * '':help /\zs''
  * '':%!sort'' pošle celý soubor do filtru sort
  * perldo poznámka
    * '':perldo $_=' ' if $x{$_}++'' <- zahodí všechny duplikátní řádky v souboru
    * '':perldo BEGIN {%x={}} $_=' ' if $x{$_}++'' <- řeší problém s reinicializací hashe
    * ale raději '':sort u''
  * Otevření množství souborů s použitím globů '':n *glob*''
  * Search & replace přes soubory z argumentů '':argdo s/regex/replac/ | update''
  * '':bufdo bd'' vykoná příkaz v každém bufferu
  * visincrPlugin -> ''^v:I'' a ''^v:II'' automatické číslování (vytváření seznamů)
  * Super trik s global a norm
    * V command mode lze používat normal mode příkazy pomocí '':norm'' ('':help norm'')
    * Vykonání příkazu nad řádky vybranými regexem '':global'' ('':g/regex/cmd'')
    * Sjednocením předchozích můžeme aplikovat normal mode příkazy nad řádky vybranými pomocí regexů
    * '':g/^\d/norm A;'' <- přidá středník na konec všech řádků, které začínají číslicí
    * '':g/xyz/norm ddGp'' <- přesune řádky odpovídající vzoru na konec souboru
    * '':g/""/norm f"aTEXT'' <- doplní do všech dvojitých uvozovek "TEXT"
    * .. spousta možností!!!
  * Něco o regexech
    * Speciální znaky jsou jiné než v perlu nebo posix regexech, protože se očekává časté vyhledávání částí céčkových programů a tak většina znaků nemá speciální význam protože by se jinak musel pořád escapovat
    * Speciální jsou např ''* \+ \{x,y} \( \) [ ]''
    * Lze přepínat mezi magií a nemagií (magic, nomagic)
    * Při definování regexu lze použít ''\v'' (magic) -> vše kromě ''[a-zA-Z0-9_]'' se stane speciálnimi znaky
      * nomagic je ''\V''
      * lze psát věci jako '':s/\v^%(foo){1,3}(.+)bar$/\1/''
    * Specifikování ''\c'' zapne case sensitivity
      * '':help /\v''
    * Operátor ''\_.'' (náhrada za ''.'')
      * '':help \_.''
      * '':%s@<body>\v(\_.+)\V</body>@\1@''
        * (delimiter může být ''@'')
    * možnost použití "very magic"
  * Pokud je použito '':set wrap'' tak jedna řádka může být na více visuálních řádcích -> pohyb kurzorem je skokový -> ''gj'', ''gk'' skáče po zobrazených řádcích a ne po skutečných
  * Split okna lze pomocí '':split'' nebo jednodušeji pomocí ''^ws'' ([w]indow[s]plit) (ale pozor na ''^w^s'' -> zablokuje terminál -> ''^q'')
  * Přesunout okno jako nový tab -> ''^wT''
  * '':x'' provede uložení souboru (pokud se změnil) a zavření souboru
  * '':s/\<./\U&*/g'' nahradí všechna první písmena slov za velká, použito je ''&'' které reprezentuje celý match, ''\<'' které reprezentuje levý kraj slova, ''\U'' které mění na Uppercase
  * V command módu lze vložit obsah registru pomocí ''^r<regname>''
  * Rolodex Vim
    * Pokud máme malý display a chceme editovat více souborů ve Vimu, mohl by se nám hodit "Rolodex Vim"
    * [[http://groups.google.com/group/vim_use/browse_thread/thread/bb559632ea5bef46/b97bf7e26c36baa8?lnk=gst&q=rolodex#b97bf7e26c36baa8|rolodex - vim_use]]
    * <code>if has("windows")
     if has("autocmd")
       augroup rolodex
         au VimEnter * set noea wmh=0 wh=99999 hh=99999
       augroup vimrclocal
     else
         set noea wmh=0 wh=99999 hh=99999
     endif
endif </code>
  * Interaktivní konfigurace Vimu '':options''
  * '':set wildmenu'' bude zobrazovat speciální řádek při použití tabulátoru na doplnění cesty v příkazovém módu. Tento řádek bude zobrazovat obsah adresáře a lze se v něm pohybovat doleva (následující položka), doprava (předcházející položka), nahoru (o jeden adresář nahoru), dolů (vstup do adresáře pod kurzorem)
  * ''ci''' nebo ''ci"'' ('':help iquote'') změní obsah uvozovek s kurzorem kdekoliv uvnitř.
  * ''gi'' přejde do INSERT módu tam, kde jsme skončili
  * ''q:'' otevře historii příkazů. Ta lze editovat stejně jako každý jiný buffer a při stisknutí ''Enter'' se spustí příkaz pod kurzorem. ''q/'' a ''q?'' je historie vyhledávání. Opět lze použité regulární výrazy měnit a znovu používat. Do historie vyhledávání se lze dostat z vyhledávacího módu pomocí ''^f''. Pro snadnější vyhledávání je vhodné používat za příkazy komentáře (''" komentar'')
  * ''^v''vybereme blok textu ve sloupcovém režimu a pomocí ''y'' jej nakopírujeme do schránky. Pastnutí do textu provedeme pomocí ''p'' nebo ''P''. Pokud chceme cílový text přepsat (overwrite), použijeme ''1vp'', kde ''1v'' vybere stejně velkou oblast jako byla vybrána naposledy a ''p'' do ní přepíše obsah schránky.
  * ''^k N S'' v INSERT módu vytvoří nerozdělitelnou mezeru (nbps).