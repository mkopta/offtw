====== Qemu ======

===== Co to je =====

Virtualizační nástroj. Něco jako VirtualBox, ale narozdíl od VirtualBoxu to funguje.

===== Tvorba image =====

''qemu-img create -f qcow2 arch.img 2G''

===== Instalace z CD/DVD =====

''qemu -cdrom ~/archlinux-2009.08-netinstall-i686.iso -hda arch.img -m 256 -boot d''

===== KVM a monitor =====

Pokud máte nahraný modul ''kvm'' a ''kvm-intel'' (popřípadě ''kvm-amd'') a Váš procesor podporuje ''vmx'' nebo ''svm'', můžete používat hardwarovou akceleraci virtualizace v Qemu.

  * ''egrep '^flags.*(vmx|svm)' /proc/cpuinfo''
  * ''qemu-kvm -hda virt.img -vga std -monitor stdio''

Bez parametru ''-monitor'' je možné za běhu Qemu použít klávesovou zkratku ''ctrl-alt-2'' pro přepnutí na konzoli Qemu (a ''ctrl-alt-1'' zpět), ve které lze různě manipulovat s pamětí virtuálního počítače, jeho periferiemi a například zasílat speciální klávesové zkratky (''ctrl-alt-f1''). Volba ''-monitor stdio'' přepne konzoli Qemu na terminál, ze kterého je Qemu spouštěn.

===== Spouštění =====

  - ''modprobe kqemu'' 
  - ''qemu -hda arch.img -m 256 -kernel-kqemu''


