====== Wmii ======

===== První krok - Start =====
<code>

> cat .xinitrc 
#
# ~./xinitrc
#
# Executed by startx
#

# Set up background
feh --bg-center ~/.bg
# Set up keyboard
xmodmap /home/dum8d0g/.xmodmap &
xset r rate 200 50 &
# Start monitoring
conky &
# Start xscreensaver
#xscreensaver -no-splash &
# Start openbox
#exec openbox
exec wmii
</code>

po spuštění příkazu wmii v konzoli se nastartují Xka a wmii.


===== Základní ovládání =====

První věc - všechno jsou klávesové zkratky. Klikání neni. Takže -
  * mod enter - spustí terminál
  * mod p     - vyber programu pro spusteni (funguje completing a litani sipkama)
  * mod {hjkl} - prepinani mezi okny
  * mod {123...x} - prepinani mezi plochami (dynamicky vytvarene)
  * mod shift {123...x} - poslani aktualniho okna na jinou plochu
  * mod shift c - zabije aktuálni okno
mod je v základním nastavení klávesa lalt (left alt).