====== CouchDB ======

==== vytvoření nového uživatele ====
<code bash>
export HOST='localhost:5984'
# heslo je "secret", sul je "1"
echo -n "secret1" | shasum  # vrati 00cafd126182e8a9e7c01bb2f0dfd00496be724f
curl -X POST $HOST/_users -d '{"_id":"org.couchdb.user:franta","type":"user","name":"franta","roles":[],"password_sha":"00cafd126182e8a9e7c01bb2f0dfd00496be724f", "salt":"1"}' -H "Content-Type: application/json" -u "admin:supersecret"
</code>
