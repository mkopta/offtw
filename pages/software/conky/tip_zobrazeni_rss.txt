====== Zobrazení RSS novinek ======
===== conkyrc =====
V conkyrc pouze voláme externí script, který udělá všechnu práci.

.conkyrc:
<code>
TEXT
${execi 600 /cesta/k/externimu/scriptu}
</code>
===== Script pro vypsání titulků rss =====
simple-rss-reader.pl:
<code perl>
#!/usr/bin/env perl

###################
# Name: simple rss reader
#
# Original script: by Jeff Israel
# downloaded from http://howto.wikia.com
# 
# Licence: GPL
#
# #################

# turn warnings on
use warnings;
# use strict checking
use strict;
use LWP::Simple;

# limit width of item titles
my $maxTitleLenght = 45;

# list of rss pages to fetch
# format: <limit>@<title>@<adress>
# where:
# <limit> - max. number of entries printed
#   mandatory (if you omit this, perl will probably use number 0)
# <title> - title of page to be printed before its entries
#   optional (if empty string, then no title will be printed)
# <adress> - web adress od rss feed
#   mandatory ...
my @rssPage = (
        '5@@http://martin.kopta.eu/trash/wiki//feed.php',
        '3@ANBU@http://rss.a.scarywater.net/tag/anbu.rss',
        );

# main loop
for my $rssPage (@rssPage) {

# parse the page entry 
        my ($numLines ,$pageTitle ,$rssAdress) = split /@/, $rssPage;
# download the page
        my $pageCont = get($rssAdress);
# get rid of newlines
        $pageCont =~ s/\n//g;
# get rid of anything what commes before first <item>
        $pageCont =~ s/^.*?\<item(\s[^\>]*)?\>//;
# set number of printed lines to 0
        my $printedLines = 0;
# print title of rss page (if any)
        print "$pageTitle\n" if $pageTitle;
# loop for entries in actual page
        while (($printedLines < $numLines) && ($pageCont =~ /\<title\>/)) {
# copy remaining content 
                my $line = $pageCont;
# extract title of 'first' entry
                $line =~ s/.*?\<title\>//;
                $line =~ s/\<\/title\>.*//;
# limit string length
                $line = substr($line, 0, $maxTitleLenght);
# print out the title
                print "- $line \n";
# get rid of 'first' entry
                $pageCont =~ s/.*?\<title\>.*?\<\/title\>//;
                $printedLines++;
        }


}

# EOF
</code>