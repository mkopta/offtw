====== Ovládání wi-fi ======

===== Nástroje =====

  * iwconfig
  * ifconfig
  * ip
  * wpa_supplicant
  * ...

(balíky - iproute, wireless_tools, wpa_supplicant, ..)

===== Konfigurace =====

[[conf:wpa_supplicant|wpa_supplicant.conf]] ukázková konfigurace pro [[http://www.eduroam.org/|Eduroam]]


===== Základní příkazy =====

<code bash>
# vypnuti sitoveho rozhrani
ip l s dev wlan0 down 
# zapnuti sitoveho rozhrani
ip l s dev wlan0 up
# zahozeni veskere konfigurace sitoveho rozhrani
ip a f dev wlan0
# vymazání routovací tabulky
ip ro flush all #asi, ip ro help
# přehled nastavení ip adresy atp
ifconfig
# nebo
ip l show dev wlan0
# přehled nastavení bezdrátových rozhraní 
iwconfig
# nastavení bezdrátového rozhraní na síť 'supersit' s primym pristupem
# (nekdy je nutne opakovat, pripadne zkusit s vypnutym, zapnutym, nakonfigurovanym
#  i nenakonfigurovanym zarizenim, kvuli ne vzdy dobre fungujicim driverum)
iwconfig wlan0 essid supersit mode ad-hoc
</code>