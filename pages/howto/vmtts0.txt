====== Jak na seriovou konzoli virtualnich stroju ======

Nejprve si spustime vm s emulovanou seriovou konzoli (chardev a device)

<code>
kvm \
        -enable-kvm \
        -m 128 \
        -boot d \
        -cdrom systemrescuecd-x86-2.4.0.iso \
        -chardev pty,id=charserial0 \
        -device isa-serial,chardev=charserial0,id=serial0 \
        -vnc :0
</code>

Guest musi mit nakonfigurovanou seriovou konzoli. Pokud nema, je potreba ji nakonfigurovat. Pro linux vetsinou do /etc/inittab pridame:

<code>
c0:2345:respawn:/sbin/agetty 9600 ttyS0 vt100
</code>
Kazde distro ma trochu jiny format inittab. v pripade existence souboru /etc/securetty soubor upravime. Pro nacteni prave ulozene konfigurace pouzijeme

<code>
init q
</code>

Pro ziskavani vypisu kernelu muzeme pridat i na kernel line do grubu parametr

<code>
console=ttyS0,9600 console=tty
</code>

Pri spusteni virtualu nam qemu vyhodi lokaci seriove konzole virtualu v hostiteli
<code>
char device redirected to /dev/pts/2
</code>

Ted staci jenom vzit program na pripojovani se na seriovou konzoli a pripojit se. Pouzit se da minicom (sracka) nebo treba picocom (trochu lepsi)
<code>
minicom -p /dev/pts/2
</code>

<code>
picocom -f n -p n -b 9600 -i -r -l /dev/pts/2
</code>

(na BSD se pouziva "cu")