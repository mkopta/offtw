====== Sitovani virtualnich stroju ======

<code>
brctl addbr br0
# (deprecated) tunctl -b -u mk -t vnet0
ip tuntap add dev vnet0 mode tap user mk group kvm
# (deprecated) tunctl -b -u mk -t vnet1
ip tuntap add dev vnet1 mode tap user mk group kvm
brctl addif br0 vnet0
brctl addif br0 vnet1
ip a a 192.168.1.1/24 dev br0
iptables -t nat -A POSTROUTING -s 192.168.1.0/24 ! -d 192.168.1.0/24 -j MASQUERADE
# (or more specific) iptables -t nat -A POSTROUTING -s 192.168.1.0/24 ! -d 192.168.1.0/24 -o eth0 -j SNAT --to 34.33.102.12
qemu --enable-kvm -cdrom microcore-current.iso -m 64 -net nic,macaddr=54:52:00:00:00:00 -net tap,ifname=vnet0
qemu --enable-kvm -cdrom microcore-current.iso -m 64 -net nic,macaddr=54:52:00:00:00:01 -net tap,ifname=vnet1
sysctl -w net.ipv4.conf.all.forwarding=1
ip l s dev vnet0 up
ip l s dev vnet1 up
ip l s dev br0 up
</code>
