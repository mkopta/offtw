====== Domácí Cinzano ======

7 l vody + 3 kg cukru svaříme a necháme vychladit.

  * 16 dag droždí
  * 2 vanilky
  * 30 hřebíčků
  * 2 1/2 citrónu (ostrouhat kůru, rozkrájet a potom přidat)
  * 30 ks nového koření
  * 1/2 lžičky muškátového květu
  * 1/2 lžičky muškátového oříšku
  * 1/2 lžičky tymiánu
  * 1/2 zázvoru
  * kousek (asi 3cm) skořice
  * 1 kg rýže - neprat

Vše zalijeme vychlazenou vodou a necháme 6 týdnů kvasit. Potom stočíme a necháme 14 dní ve druhé nádobě. Nakonec nalijeme do lahví. 