====== OpenBSD ======

OpenBSD je svobodný unix z rodiny operačních systémů BSD pocházejích z dávných dob unixové historie. Zaměřuje se především na bezpečnost, svobodu, dodržování norem (POSIX, ANSI, ..) a na striktní licencování.

===== Výhody OpenBSD =====

  * Vysoká bezpečnost
  * Velmi odborná komunita
  * Vysoká portabilita (mnoho podporovaných platforem)

===== Nevýhody OpenBSD =====

  * Zavrhování binárních blobů vede k nepodporování mnoha ovladačů od NVIDIA, ATI, Intel, Atheros a dalších
  * Velmi malá komunita
  * Menší množství nativního software

===== Náhodné poznámky =====

==== Konfigurace wifi ====

Pro intel 3945 je tu wpi(4). Firmware od intelu není dodávaný s distribucí, ale lze jej stáhnout a nainstalovat příkazem (z manuálové stránky wpi):
<code>
pkg_add -v http://damien.bergamini.free.fr/packages/openbsd/wpi-firmware-3.1.tgz
</code>

Pak je potřeba nakonfigurovat wifi pro připojení přes WPA-PSK (pro další konfigurace viz wpI(4)). 

<code bash>
-bash-3.2# cat /root/wifi 
#!/bin/sh
set -x
ifconfig wpi0 nwid Kopta wpa wpapsk 0x0000000000000000000000000000000000000000000000000000000000000000
dhclient wpi0
-bash-3.2# 
</code>

Místo teho dlouhého hexa kódu je má být výstup příkazu wpa-psk(8).

Pokud je konfigurace wifi provedena po použití ethernetové sítě, wifi nefunguje korektně (nepátral jsem po přesné příčině). Po restartu počítače a okamžité konfiguraci wifi jede bezdrát přes WPA-PSK bez problému.

==== Správa software ====

Software se instaluje pomocí ''pkg_add(1)''. Příklad:
<code>
pkg_add -v bash
</code>

Nainstaluje bourne-again shell (s verbose módem). Pro instalaci software je ale potřeba definovat odkud se má tahat. To se dělá proměnnou PKG_PATH (nastavit třeba v .bashrc)

<code>
export PKG_PATH="ftp://ftp.openbsd.org/pub/OpenBSD/4.5/packages/i386/"
</code>

Přímým přístupem do FTP repositáře můžeme stáhnou seznam všech dostupných balíčků software.

<code>
ftp ftp://ftp.openbsd.org/pub/OpenBSD/4.5/packages/i386/
mls . packages
</code>

Počet balíčků je cca 5400.

Informace o balíčcích lze vzdáleně zjišťovat pomocí ''pkg_info(1)''.
<code>
bash-3.2$ pkg_info bash
Information for inst:bash-3.2.48

Comment:
GNU Bourne Again Shell

Description:
Bash is the GNU Project's Bourne Again SHell, an sh-compatible
command language interpreter that executes commands read from the
standard input or from a file.  Bash also incorporates useful
features from the Korn and C shells (ksh and csh).

Bash is intended to be a conformant implementation of the IEEE POSIX
Shell and Tools specification (IEEE Working Group 1003.2).

Maintainer: Christian Weisgerber <naddy@openbsd.org>

WWW: http://cnswww.cns.cwru.edu/~chet/bash/bashtop.html


bash-3.2$ 
</code>

OpenBSD má k dispozici také porty. Ty však narozdíl od FreeBSD nejsou potřeba využívat. Veškerý software je dostupný ve zkompilované podobě. Informace o portech -> ''ports(7)''


Odstranění balíčku se provádí pomocí ''pkg_delete(1)''.


Databáze balíčků je uložena v ''/var/db/pkg''. V adresáři ''/var/tmp'' se ukládají rozpracováné (právě instalované soubory balíčků). 

Pro odstranění všeho nainstalovaného software, který nepatří do základního systému lze provést
<code bash>
rm -rf /usr/local/*
rm -rf /var/db/pkg/*
</code>
Protože OpeBSD má oddělený uživatelský software a základní systém, lze toto bezproblémů provést. V případě potřeby takovéto akce je dobré si vypsáním ''/var/db/pkg'' zálohovat seznam nainstalovaných balíků pro jejich případnou obnovu.


Aplikace v portech a v balíčcích neprošli bezpečnostním auditem tak jako základní systém a OpenBSD se nezaručuje za jejich plnou bezpečnost. Proto je například vhodnější používat Apache httpd verze 1, který je součástí base systému než httpd 2, který je dostupný v balíčcích. Stejně tak gcc 3.2 versus gcc 4.2 a další.

==== Grafika ====

OpenBSD dodává nějaký základní window manager (FVWM). Pro přidání něčeho jiného (třeba openbox) stačí:
<code>
pkg_add -v openbox
</code>

Samozřejmě je potřeba nakonfigurovat inicializační soubor ''~/.xinitrc''

<code bash>
#!/bin/sh
xsetroot -solid gray30
exec openbox
</code>

Poté lze spustit xorg pomocí příkazu ''startx''. Pokud něco nenaběhne a výpis xorgu v konzoli bude hlásit cosi o refused, může pomoci smazání ''rm ~/.Xauth*''


==== Správa démonů ====

V OpenBSD není žádné ''/etc/init.d'' ani ''/etc/rc.d''. 

  * Spuštění démona sshd: ''/usr/sbin/sshd''
  * Vypnutí démona sshd: ''pkill sshd''
  * Restartování démona sshd: ''pkill -HUP sshd''

Spouštění démonů po startu -> ''/etc/rc.conf''

==== Grub entry ====

<code>
# OpenBSD
title  OpenBSD 4.5
root   (hd0,1a)
chainloader +1
</code>


==== Sysctl ====

Všelijaké nastavování: sysctl + /etc/sysctl.conf


==== Mount flashdisku ====

  - Zasunout flashdisk
  - Počkat pár vteřin
  - Spustit dmesg (pod rootem) a najít na jaké blokové zařízení je HW inicializován (třeba sd1)
  - Pokud neexistuje mountpoint, vytvořit ''mkdir /mnt/fl''
  - ''mount /dev/sd1i /mnt/fl'' (pokud je flashdisk FS FAT, použít mount_msdos)

==== Barevné ls ====

<code>
pkg_add -v bash
chsh
echo 'alias ls="gls --color"' >> ~/.bashrc
</code>


==== Čtyřková řada gcc/g++ ====

  * pkg_add -v g++4* # doplnit podle obsahu archivu
  * novější gcc/g++ pak má v systému název 'eg++' a 'egcc' !!


==== Barevný Vim ====


Nutno nastavit ''export TERM="xterm-color"'' v bashrc nebo někde..


==== PHP mail() a chroot ====

V defaultnim zachrootovanem Apache s PHP nefunguje fce mail(). Je potreba zajistit, aby existoval ''/var/www/usr/sbin/sendmail'' (''/var/www/bin/femail'') a vytvorit ve ''/var/www/etc'' soubory ''resolv.conf'' a ''hosts''. Take je nutno nakopirovat ''/bin/sh'' do ''/var/www/bin/''.
===== Odkazy =====

  * [[http://undeadly.org/|OpenBSD Jurnal (blog)]]
  * [[http://www.openbsd.org/lyrics.html|OpenBSD release songs]]
  * [[http://undeadly.org/cgi?action=article&sid=20090715034920|BSD load demystified]]

===== Jak to vidí dum8d0g =====

==== Co se mi na OpenBSD líbí ====

  * Jde o čistokrevný unix
  * Je určený pro profesionály -> částečná ignorace nezajímavých témat (desktop enviroment atp.)
  * Funguje wifi
  * V manuálových stránkách jsou skutečně užitečné informace
  * Systém je velmi malý. S funkčním grafickým rozhraním, firefoxem a hromadou programů zabírá méně než jeden a půl gigabajtu
  * Pěkný přístup k práci s démony
  * Jednoduchý balíčkovací systém
  * Oddělení základního systému a uživatelského software umožňuje jednoduchý flush v případě potíží
  * Možnost, ale ne nutnost využití portů

==== Nevyřešené otázky ====

  * <del>Jak na dynamické linkování? ''-ldl'' problém</del>
    * dynamic linker je implicitní
  * Jak jsou ''logl()'', ''log10l()'', .. funkce?
  * <del>Jak jednoduše ovládat hlasitost (nějaký ncurses mixér) ?</del>
    * man mixerctl + troska programovani
  * <del>Jak zařídit unicode v xterm?</del>
    * prekompilovat s podporou + nejake dalsi triky
  * <del>Jak mít 256 barev ve Vimu (i ve Vimu pod screen)?</del>
    * prekompiloval xterm s --enable-256
  * <del>Jak doinstalovat prename?</del>
    * cp prename ~/lo/bin
  * <del>Jak doinstalovat vocatra?</del>
    * make install (prefix == ~/lo)
  * <del>Jak nakonfigurovat ntpd?</del>
    * man ntpd
  * Jak jednoduše modifikovat software (patchování, jiný configure, ..)?
  * Jak ovládat nastavení sítě po startu?
  * Jak mountovat jiné oddíly? (ne bsdlabels ale jiné oddíly disku, které nejsou pod správou OpenBSD)
  * Jak vytvářet nové balíky?
  * Jak upravovat balíky?
  * <del>Jak aktualizovat ports?</del>
    * kompletně popsané v OpenBSD FAQ
  * <del>Jak aktualizovat systém?</del>
    * kompletně popsané v OpenBSD FAQ
  * <del>Jak nastavit locales?</del>
    * locales nejsou