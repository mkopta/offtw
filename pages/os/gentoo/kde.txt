====== KDE v Gentoo ======
Momentálně je v portage verze 4.2.2. Nejjednodušeji jí nainstalujeme odmaskováním balíku kde-meta pomocí autounmask.

<code>
autounmask kde-base/kde-meta-4.2.2 && emerge -av kde-meta
</code>

Existuje také overlay kde-testing, ve kterém můžete nalézt buď 9999 balíky nebo kde ve verzi 4.2.69.

<code>
layman -a kde-testing
update-eix
autounmask kdebase-startkde-4.2.69</code>#bacha - neodmaskuje cele KDE, ale jen nejpotrebnejsi soucasti

Pokud mate novější portage (2.2pre a víc) tak můžete k nainstalování kde z overlaye takzvané sety:
<code>
eix -n --only-names kde-base/ | sed 's/^\(.*\)$/=\1-4.2.69/' >> /etc/portage/package.unmask/kde-4.3
eix -n --only-names kde-base/ | sed 's/^\(.*\)$/=\1-4.2.69 ~x86/' >> /etc/portage/package.keywords/kde-4.3
emerge -av kde-4.3</code>

#více viz sekce [[os:gentoo:tipy_triky|Tipy & Triky]]
