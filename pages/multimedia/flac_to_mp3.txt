====== Převod .flac do .mp3 ======
Přepneme se do adresáře s hudbou, kterou chceme převést a spustíme tento jednoduchý příkaz:
<code bash>for file in *.flac; do $(flac -cd "$file" | lame -h -b 320 - "${file%.flac}.mp3"); done </code>

p.s. - pozor, nepřevede id3 tagy -> doporučuju následně použít nějaký tagovač typu Picard...

===== Převod včetně id3 tagů =====
Pro převod včetně id3 tagů můžeme použít následující skript:
<code bash>
#!/bin/bash
FLAC=$1
MP3="${FLAC%.flac}.mp3"
[ -r "$FLAC" ] || { echo can not read file \"$FLAC\" >&1 ; exit 1 ; } ;
metaflac --export-tags-to=- "$FLAC" | sed 's/=\(.*\)/="\1"/' >tmp.tmp
cat tmp.tmp
. ./tmp.tmp
rm tmp.tmp
flac -dc "$FLAC" | lame -b 320 -h --tt "$Title" \
--tn "$Tracknumber" \
--tg "$Genre" \
--ty "$Date" \
--ta "$Artist" \
--tl "$Album" \
--add-id3v2 \
- "$MP3"
</code>
