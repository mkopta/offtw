====== Cytání video výstupu z programu gource ======
===== Uložení renderovaného videa =====
<code>
gource -640x480 --stop-position 1.0 -a 2 --disable-progress --output-framerate 30 --multi-sampling --output-ppm-stream - /cesta/k/repu | ffmpeg -y -r 30 -vcodec ppm -f image2pipe  -i - -vcodec ffvhuff vystup.avi
</code>
===== Sestavení roury přímo do x264 =====
<code>
gource -1280x720 -b 111111 --stop-position 1.0 -a 2 --disable-progress --output-framerate 25 --multi-sampling --output-ppm-stream - | ffmpeg -y -r 25 -vcodec ppm -f image2pipe  -i - -vcodec rawvideo -f rawvideo -pix_fmt yuv420p - | x264 --preset fast --tune animation --fps 25 --ssim -o video.264 - 1280x720 2> x264.out
</code>